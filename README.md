# Coding Challenge - Mykola Shlyakhtun

## Backend

Before launch backend sql connection string should be modified in file.

```
backend\src\CodingChallenge.API\appsettings.json 
```

## Frontend

Before launch .env file should be modified to point to correct API.

### Install dependencies

```yarn```

### Launch frontend
```yarn start```

