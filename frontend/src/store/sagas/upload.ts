import {
  call,
  put,
  takeLatest
} from 'redux-saga/effects'
import {
  UPLOAD_FILE,
  UploadFileAction
} from "../types/upload";
import { uploadService } from "../../services/upload";
import { setSuccess, setError } from "../slices/upload";

const {
  uploadFile
} = uploadService()

function* uploadFileSaga({payload}: UploadFileAction) {
  try {
    yield put(setError(''));
    yield put(setSuccess(false));

    yield call(uploadFile, payload);

    yield put(setSuccess(true));
  } catch (err) {
    yield put(setError("Can't upload file"))
  }
}

function* uploadSaga() {
  yield takeLatest(UPLOAD_FILE, uploadFileSaga)
}

export default uploadSaga
