import { spawn } from 'redux-saga/effects'

import saleSaga from './sale'
import uploadSaga from "./upload";

export default function* rootSaga() {
    yield spawn(saleSaga);
    yield spawn(uploadSaga);
}
