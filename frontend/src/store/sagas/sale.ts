import {
  put,
  call,
  takeLatest
} from 'redux-saga/effects'
import { LOAD_SALES } from "../types/sale";

import {
  setError,
  setSales
} from "../slices/sale";
import { saleService } from "../../services/sale";

const {
  getVehicleSales
} = saleService()

function* loadSalesSaga() {
  try {
    yield put(setError(''));

    // @ts-ignore
    const sales = yield call(getVehicleSales);

    yield put(setSales(sales))
  } catch (err) {
    console.log('error', err);
    yield put(setError("Can't load sales"));
  }
}

function* saleSaga() {
  yield takeLatest(LOAD_SALES, loadSalesSaga)
}

export default saleSaga
