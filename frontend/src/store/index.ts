import { configureStore } from '@reduxjs/toolkit';
import rootSaga from './sagas/root'
import saleReducer from './slices/sale';
import uploadReducer from './slices/upload';
import createSagaMiddleware from 'redux-saga'

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: {
    sale: saleReducer,
    upload: uploadReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false}).concat(sagaMiddleware),
})

sagaMiddleware.run(rootSaga);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
