export const UPLOAD_FILE = 'UPLOAD_FILE';

export interface UploadFileAction {
  payload: File;

  type: typeof UPLOAD_FILE;
}


export interface UploadState {
  success?: boolean;
  error: string;
}
