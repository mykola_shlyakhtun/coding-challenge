import { Sale } from "../../typings/sale";

export const LOAD_SALES = 'LOAD_SALES'

export interface LoadSalesAction {
    payload: null
    type: typeof LOAD_SALES
}

export interface SaleState {
    sales: Array<Sale>,
    error: string
}

