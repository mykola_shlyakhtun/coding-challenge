import {
  UPLOAD_FILE,
  UploadFileAction
} from "../types/upload";

export const uploadFile = (payload: File): UploadFileAction => ({
  payload,
  type: UPLOAD_FILE,
})
