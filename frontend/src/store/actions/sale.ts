import {
  LOAD_SALES,
  LoadSalesAction
} from "../types/sale";

export const loadSales = (payload = null): LoadSalesAction => ({
  payload,
  type: LOAD_SALES,
})
