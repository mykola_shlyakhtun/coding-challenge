import {
  createSlice,
  PayloadAction
} from '@reduxjs/toolkit';
import { UploadState } from "../types/upload";

const initialState: UploadState = {
  success: undefined,
  error: ''
};

export const uploadSlice = createSlice({
  name: 'upload',
  initialState,
  reducers: {
    setSuccess: (state, action: PayloadAction<boolean>) => {
      state.success = action.payload;
    },
    setError: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
    }
  },
});

export const { setSuccess, setError } = uploadSlice.actions;

export default uploadSlice.reducer;
