import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {SaleState} from "../types/sale"
import { Sale } from "../../typings/sale";

const initialState: SaleState = {
  sales: [],
  error: ''
};

export const saleSlice = createSlice({
  name: 'sale',
  initialState,
  reducers: {
    setSales: (state, action: PayloadAction<Array<Sale>>) => {
      state.sales = action.payload;
    },
    setError: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
    }
  },
});

export const { setSales, setError } = saleSlice.actions;

export default saleSlice.reducer;
