import { Heading } from "@chakra-ui/react";

export const Home = () => (
  <div>
      <Heading as='h1'>Home</Heading>
  </div>
)
