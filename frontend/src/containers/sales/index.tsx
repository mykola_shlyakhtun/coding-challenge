import {
  useDispatch,
  useSelector
} from "react-redux";
import { useEffect } from "react";
import { loadSales } from "../../store/actions/sale";
import { RootState } from "../../store";
import { SalesTable } from "./components/SalesTable";
import { NoSales } from "./components/NoSales";
import {
  Alert,
  AlertIcon
} from "@chakra-ui/react";

export const Sales = () => {
  const sales = useSelector((state: RootState) => state.sale.sales);

  const error = useSelector((state: RootState) => state.sale.error);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(loadSales())
  }, []);

  if (sales.length === 0) {
    return (
      <NoSales/>
    )
  }

  if (error) {
    return (
      <Alert status='error'>
        <AlertIcon />
        {error}
      </Alert>
    )
  }

  return (
    <SalesTable sales={sales}/>
  )
}
