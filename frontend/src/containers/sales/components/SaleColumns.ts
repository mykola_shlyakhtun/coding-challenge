import { Sale } from "../../../typings/sale";
import dayjs from "dayjs";

interface SaleColumn {
  name: keyof Sale;
  title: string;
  get: (value: string) => string;
}

const defaultGet = (value: string) => value;

export const SaleColumns: Array<SaleColumn> = [
  {
    name: 'dealNumber',
    title: 'Deal Number',
    get: defaultGet
  },
  {
    name: 'customerName',
    title: 'Customer Name',
    get: defaultGet
  },
  {
    name: 'dealershipName',
    title: 'Dealership Name',
    get: defaultGet
  },
  {
    name: 'vehicle',
    title: 'Vehicle',
    get: defaultGet
  },
  {
    name: 'price',
    title: 'Price',
    get: (value: string) => `CAD$${parseFloat(value).toFixed(2)}`
  },
  {
    name: 'date',
    title: 'Date',
    get: (value: string) => {
      return dayjs(value).format('MM/DD/YYYY');
    }
  }
]
