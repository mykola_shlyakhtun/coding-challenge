import {
  Table,
  Tbody,
  Td,
  Tfoot,
  Th,
  Thead,
  Tr
} from "@chakra-ui/react";
import { SaleColumns } from "./SaleColumns";
import { Sale } from "../../../typings/sale";
import { FC } from "react";

interface SalesTableProps {
  sales: Array<Sale>
}

export const SalesTable: FC<SalesTableProps> = ({ sales }) => (
  <Table variant='simple'>
    <Thead>
      <Tr>
        {
          SaleColumns.map((c) => (
            <Th key={c.name}>{c.title}</Th>
          ))
        }
      </Tr>
    </Thead>
    <Tbody>
      {
        sales.map(s => (
          <Tr key={s.id}>
            {
              SaleColumns.map((c) => (
                <Td key={c.name}>{c.get(s[c.name])}</Td>
              ))
            }
          </Tr>
        ))
      }
    </Tbody>
    <Tfoot>
      <Tr>
        {
          SaleColumns.map((c) => (
            <Th key={c.name}>{c.title}</Th>
          ))
        }
      </Tr>
    </Tfoot>
  </Table>
)
