import {
  useDispatch,
  useSelector
} from "react-redux";
import { uploadFile } from "../../store/actions/upload";
import { Form } from "./components/Form";
import {
  Alert,
  AlertIcon
} from "@chakra-ui/react";
import { RootState } from "../../store";
import { useEffect } from "react";
import { setSuccess } from "../../store/slices/upload";


export const Upload = () => {
  const success = useSelector((state: RootState) => state.upload.success);
  const error = useSelector((state: RootState) => state.upload.error);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setSuccess(false));
  }, []);

  const handleUpload = (file: File) => {
    dispatch(uploadFile(file));
  }

  return (
    <div>
      <Form onUpload={handleUpload} />

      {
        success && (
          <Alert status='success' mt={6}>
            <AlertIcon />
            Data uploaded to the server. Fire on!
          </Alert>
        )
      }

      {
        error && (
          <Alert status='error' mt={6}>
            <AlertIcon  />
            {error}
          </Alert>
        )
      }

    </div>
  )
}
