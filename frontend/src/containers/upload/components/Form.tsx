import {
  FC,
  useState
} from 'react'
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel
} from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { FileUpload } from "./FileUpload";
import { FiUpload } from "react-icons/fi";

type FormProps = {
  onUpload: (file: File) => void;
}

type FormValues = {
  file: FileList
}

export const Form:FC<FormProps> = ({onUpload}) => {
  const [submitting, setSubmitting] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm<FormValues>()
  const onSubmit = handleSubmit((data) => {
    setSubmitting(true);

    const file = data.file.item(0);

    if (file) {
      onUpload(file);
    }

    setSubmitting(false);
  })

  const validateFiles = (value: FileList) => {
    if (value.length < 1) {
      return 'Files is required'
    }
    for (const file of Array.from(value)) {
      const fsMb = file.size / (1024 * 1024)
      const MAX_FILE_SIZE = 10
      if (fsMb > MAX_FILE_SIZE) {
        return 'Max file size 10mb'
      }
    }
    return true
  }

  return (
    <form onSubmit={onSubmit}>
      <FormControl isInvalid={!!errors.file} isRequired>
        <FormLabel>{'File input'}</FormLabel>

        <FileUpload
          accept='.csv'
          multiple
          register={register('file', { validate: validateFiles })}
        >
          <Button leftIcon={<FiUpload />}  colorScheme='cyan' variant='outline' size="lg">
            Select file
          </Button>
        </FileUpload>

        <FormErrorMessage>
          {errors.file && errors?.file.message}
        </FormErrorMessage>
      </FormControl>

      <Button
        mt={6}
        colorScheme='cyan'
        isLoading={submitting}
        type='submit'
      >
        Submit
      </Button>
    </form>
  )
}
