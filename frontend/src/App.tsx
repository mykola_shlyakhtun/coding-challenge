import React from 'react';
import Layout from "./components/Layout";
import {
  Routes,
  Route
} from "react-router-dom";
import { Home } from "./containers/Home";
import { Sales } from "./containers/sales";
import { Upload } from "./containers/upload";

function App() {
  return (
    <Layout>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="upload" element={<Upload />} />
        <Route path="sales" element={<Sales />} />
      </Routes>
    </Layout>
  );
}

export default App;
