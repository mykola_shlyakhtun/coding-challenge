import { IconType } from "react-icons";
import {
  FiHome,
  FiUpload
} from "react-icons/fi";
import { BsTable } from "react-icons/all";

interface LinkItemProps {
  name: string;
  to: string;
  icon: IconType;
}

export const LinkItems: Array<LinkItemProps> = [
  { name: 'Home', icon: FiHome, to: "/" },
  { name: 'Upload', icon: FiUpload, to: "/upload" },
  { name: 'Sales', icon: BsTable, to: "/sales" }
];
