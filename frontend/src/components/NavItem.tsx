import {
  Flex,
  FlexProps,
  Icon,
} from "@chakra-ui/react";
import { IconType } from "react-icons";
import React, { ReactText } from "react";
import { Link } from "react-router-dom";

interface NavItemProps extends FlexProps {
  icon: IconType;
  to: string;
  children: ReactText;
}

export const NavItem = ({ icon, to, children, ...rest }: NavItemProps) => (
  <Link to={to} style={{ textDecoration: 'none' }}>
    <Flex
      align="center"
      p="4"
      mx="4"
      borderRadius="lg"
      role="group"
      cursor="pointer"
      _hover={{
        bg: 'cyan.400',
        color: 'white',
      }}
      {...rest}>
      {icon && (
        <Icon
          mr="4"
          fontSize="16"
          _groupHover={{
            color: 'white',
          }}
          as={icon}
        />
      )}
      {children}
    </Flex>
  </Link>
);
