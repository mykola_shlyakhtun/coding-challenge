export interface Sale {
  id: string;
  dealNumber: string;
  customerName: string;
  dealershipName: string;
  vehicle: string;
  price: string;
  date: string;
}
