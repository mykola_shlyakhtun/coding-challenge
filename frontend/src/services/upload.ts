const ROOT_URL = process.env.REACT_APP_API_URL

const UPLOAD_URL = `${ROOT_URL}/Upload`

export const uploadService = () => {
  const uploadFile = async (file: File): Promise<any> => {

    const data = new FormData();
    data.append('file', file);

    return await fetch(UPLOAD_URL, { method: 'POST', body: data });
  }

  return {
    uploadFile
  }
}
