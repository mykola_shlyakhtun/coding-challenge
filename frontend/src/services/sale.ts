const ROOT_URL = process.env.REACT_APP_API_URL

const SALES_URL = `${ROOT_URL}/Sales`

export const saleService = () => {
  const getVehicleSales = async (): Promise<any> => {

    const response = await fetch(SALES_URL)
    return await response.json();
  }

  return {
    getVehicleSales
  }
}
