﻿using CodingChallenge.Entities;
using CodingChallenge.Entities.Models;
using CodingChallenge.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CodingChallenge.Repository
{
    public class VehicleSaleRepository : RepositoryBase<VehicleSale>, IVehicleSaleRepository
    {
        public VehicleSaleRepository(RepositoryContext repositoryContext)
           : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<VehicleSale>> GetAllSalesAsync(bool trackChanges = false) => await FindAll(trackChanges).ToListAsync();

        public Task<string> GetVehicleSoldMostOften() => 
            RepositoryContext
                .VehicleSales
                .GroupBy(vs => vs.Vehicle)
                .OrderByDescending(gp => gp.Count())
                .Take(1)
                .Select(g => g.Key)
                .FirstOrDefaultAsync();

        public async Task CreateVehicleSalesAsync(IEnumerable<VehicleSale> vehicleSales) => await CreateBatchAsync(vehicleSales);               
    }
}
