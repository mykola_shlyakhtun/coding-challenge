﻿using CodingChallenge.Entities;
using CodingChallenge.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Repository
{
    public class RepositoryManager : IRepositoryManager
    {
        private RepositoryContext _repositoryContext;
        private IVehicleSaleRepository _vehicleSaleRepository;

        public RepositoryManager(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public IVehicleSaleRepository VehicleSale
        {
            get
            {
                if (_vehicleSaleRepository == null)
                    _vehicleSaleRepository = new VehicleSaleRepository(_repositoryContext);

                return _vehicleSaleRepository;
            }
        }

        public Task SaveAsync() => _repositoryContext.SaveChangesAsync();
    }
}
