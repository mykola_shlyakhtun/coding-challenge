﻿using CodingChallenge.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodingChallenge.CsvService
{
    public class CsvReader : ICsvReader
    {
        public IEnumerable<IDictionary<string, string>> Read(Stream stream, string separator)
        {
            Regex csvParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            using (var sr = new StreamReader(stream))
            {
                var header = sr.ReadLine().Split(separator);

                while (true)
                {
                    var line = sr.ReadLine();

                    if (line != null)
                    {
                        yield return csvParser.Split(line)
                            .Select((val, idx) => new { Value = val, Index = idx })
                            .ToDictionary(x => header[x.Index], x => x.Value.Trim('"').Trim());
                    }

                    else yield break;
                }
            }

        }
    }
}
