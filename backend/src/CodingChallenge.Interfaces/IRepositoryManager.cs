﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Interfaces
{
    public interface IRepositoryManager
    {
        IVehicleSaleRepository VehicleSale { get; }

        Task SaveAsync();
    }
}
