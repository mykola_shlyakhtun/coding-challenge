﻿using System.Collections.Generic;
using System.IO;

namespace CodingChallenge.Interfaces
{
    public interface ICsvReader
    {
        IEnumerable<IDictionary<string, string>> Read(Stream stream, string separator = ",");
    }
}
