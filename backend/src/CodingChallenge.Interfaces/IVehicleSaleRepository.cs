﻿using CodingChallenge.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Interfaces
{
    public interface IVehicleSaleRepository
    {
        Task<IEnumerable<VehicleSale>> GetAllSalesAsync(bool trackChanges = false);

        Task<string> GetVehicleSoldMostOften();

        Task CreateVehicleSalesAsync(IEnumerable<VehicleSale> vehicleSales);
    }
}
