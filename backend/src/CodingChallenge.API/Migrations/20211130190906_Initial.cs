﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CodingChallenge.API.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VehicleSales",
                columns: table => new
                {
                    VehicleSaleId = table.Column<Guid>(nullable: false),
                    DealNumber = table.Column<int>(nullable: false),
                    CustomerName = table.Column<string>(maxLength: 200, nullable: false),
                    DealershipName = table.Column<string>(maxLength: 100, nullable: false),
                    Vehicle = table.Column<string>(maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(8, 2)", nullable: false),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleSales", x => x.VehicleSaleId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleSales");
        }
    }
}
