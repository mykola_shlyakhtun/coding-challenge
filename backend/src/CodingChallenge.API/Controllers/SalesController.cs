﻿using AutoMapper;
using CodingChallenge.Entities.DataTransferObjects;
using CodingChallenge.Entities.Models;
using CodingChallenge.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CodingChallenge.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SalesController : ControllerBase
    {
        private readonly IRepositoryManager _repository;        
        private readonly IMapper _mapper;

        public SalesController(IRepositoryManager repository, IMapper mapper)
        {
            _repository = repository;            
            _mapper = mapper;
        }

        [SwaggerResponse(200, "Ok", typeof(VehicleSaleDto))]
        [HttpGet]
        public async Task<IActionResult> GetSales()
        {
            var dbSales = await _repository.VehicleSale.GetAllSalesAsync();

            var sales = _mapper.Map<IEnumerable<VehicleSale>, IEnumerable<VehicleSaleDto>>(dbSales);

            return Ok(sales);
        }
    }
}
