﻿using AutoMapper;
using CodingChallenge.Entities.Models;
using CodingChallenge.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CodingChallenge.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UploadController : ControllerBase
    {
        private readonly IRepositoryManager _repository;
        private readonly ICsvReader _csvReader;
        private readonly IMapper _mapper;

        public UploadController(IRepositoryManager repository, ICsvReader csvReader, IMapper mapper)
        {
            _repository = repository;
            _csvReader = csvReader;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (file == null)
            {
                throw new Exception("File is null");
            }
            if (file.Length == 0)
            {
                throw new Exception("File is empty");
            }

            using (Stream stream = file.OpenReadStream())
            {
                var csvData = _csvReader.Read(stream).ToList();

                var sales = _mapper.Map<IList<VehicleSale>>(csvData);

                await _repository.VehicleSale.CreateVehicleSalesAsync(sales);

                await _repository.SaveAsync();
            }

            return Ok();
        }
    }
}
