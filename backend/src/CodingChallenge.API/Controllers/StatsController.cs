﻿using CodingChallenge.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CodingChallenge.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StatsController : ControllerBase
    {
        private readonly IRepositoryManager _repository;        

        public StatsController(IRepositoryManager repository)
        {
            _repository = repository;            
        }      

        [HttpGet("VehicleSoldMostOften")]
        public async Task<IActionResult> GetVehicleSoldMostOften()
        {
            var vehicleSoldMostOften = await _repository.VehicleSale.GetVehicleSoldMostOften();
            
            if(string.IsNullOrEmpty(vehicleSoldMostOften))
            {
                return NotFound();
            }

            return Ok(vehicleSoldMostOften);
        }
    }
}
