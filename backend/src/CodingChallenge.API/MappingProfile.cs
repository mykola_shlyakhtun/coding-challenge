﻿using AutoMapper;
using CodingChallenge.Entities.DataTransferObjects;
using CodingChallenge.Entities.Models;
using System;
using System.Collections.Generic;

namespace CodingChallenge.API
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<IDictionary<string, string>, VehicleSale>()
                .ForMember(p => p.DealNumber, mo => mo.MapFrom(row => int.Parse(row["DealNumber"])))
                .ForMember(p => p.CustomerName, mo => mo.MapFrom(row => row["CustomerName"]))
                .ForMember(p => p.DealershipName, mo => mo.MapFrom(row => row["DealershipName"]))
                .ForMember(p => p.Vehicle, mo => mo.MapFrom(row => row["Vehicle"]))
                .ForMember(p => p.Price, mo => mo.MapFrom(row => Convert.ToDecimal(row["Price"])))
                .ForMember(p => p.Date, mo => mo.MapFrom(row => DateTime.ParseExact(row["Date"], "M/d/yyyy", null)));

            CreateMap<VehicleSale, VehicleSaleDto>();
        }
    }
}
