﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodingChallenge.Entities;
using CodingChallenge.Interfaces;
using CodingChallenge.CsvService;
using CodingChallenge.Repository;

namespace CodingChallenge.API.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services) =>
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder =>
                    builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

        public static void ConfigureCsvService(this IServiceCollection services) =>
            services.AddScoped<ICsvReader, CsvReader>();

        public static void ConfigureDBContext(this IServiceCollection services, IConfiguration configuration) =>
            services.AddDbContext<RepositoryContext>(opts =>
                opts.UseSqlServer(configuration.GetConnectionString("sqlConnection"), b => b.MigrationsAssembly("CodingChallenge.API")));

        public static void ConfigureRepositoryManager(this IServiceCollection services) =>
           services.AddScoped<IRepositoryManager, RepositoryManager>();

        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "CodingChallenge API",
                    Version = "v1",                    
                });
                c.EnableAnnotations();
                c.CustomOperationIds(
                    d => (d.ActionDescriptor as ControllerActionDescriptor)?.ActionName);
                c.UseInlineDefinitionsForEnums();
            });
        }
    }
}
