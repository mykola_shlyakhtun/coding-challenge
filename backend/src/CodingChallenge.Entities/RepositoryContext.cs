﻿using CodingChallenge.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace CodingChallenge.Entities
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
        : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {            
        }

        public DbSet<VehicleSale> VehicleSales { get; set; }        
    }
}
