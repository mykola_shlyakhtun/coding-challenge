﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CodingChallenge.Entities.Models
{
    public class VehicleSale
    {
        [Key]
        [Column("VehicleSaleId")]        
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Deal Number is a required field.")]
        public int DealNumber { get; set; }

        [Required(ErrorMessage = "Customer Name is a required field.")]
        [MaxLength(200, ErrorMessage = "Maximum length for the Customer Name is 200 characters.")]
        public string CustomerName { get; set; }

        [Required(ErrorMessage = "Dealership Name is a required field.")]
        [MaxLength(100, ErrorMessage = "Maximum length for the Dealership Name is 100 characters.")]
        public string DealershipName { get; set; }

        [Required(ErrorMessage = "Vehicle is a required field.")]
        [MaxLength(100, ErrorMessage = "Maximum length for the Vehicle is 100 characters.")]
        public string Vehicle { get; set; }

        [Required(ErrorMessage = "Price is a required field.")]
        [Column(TypeName = "decimal(8, 2)")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Date is a required field.")]
        public DateTime Date { get; set; }
    }
}
